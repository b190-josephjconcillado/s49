/**
 * https://jsonplaceholder.typicode.com/posts
 * 
 */
let posts = [];
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => {
    posts = data;
    showPosts(posts);
});

const showPosts = (posts) => {
    let postEntries ="";
    posts.forEach((post) => {
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost(${post.id})">Edit</button>
                <button onclick="deletePost(${post.id})">Delete</button>
            </div>
        `
    });
    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts',{
        method:"POST",
        body:JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId:1
        }),
        headers: {'Content-type':'application/json;charset = UTF-8'}
    }).then(response => response.json()).then(data => {
        console.log(data);
        posts.push(data);
        showPosts(posts);
    });

    document.querySelector('#txt-title').value = null;
    document.querySelector('#txt-body').value = null;

});

const editPost = (val) => {
    document.querySelector('#btn-submit-update').disabled = false;
    document.querySelector('#txt-edit-title').disabled = false;
    document.querySelector('#txt-edit-body').disabled = false;
    document.querySelector('#txt-edit-id').value = val;
    document.querySelector('#txt-edit-title').value = document.querySelector(`#post-title-${val}`).innerHTML;
    document.querySelector('#txt-edit-body').value = document.querySelector(`#post-body-${val}`).innerHTML;
}

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    fetch(`https://jsonplaceholder.typicode.com/posts/${document.querySelector('#txt-edit-id').value}`,{
        method:"PUT",
        body:JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value
        }),
        headers: {'Content-type':'application/json;charset = UTF-8'}
    }).then(response => response.json()).then(data => {
        console.log(data);
        let index = posts.findIndex(x => x.id.toString() === document.querySelector('#txt-edit-id').value);
        if(index > -1) {
            posts[index].title = data.title;
            posts[index].body = data.body;
            showPosts(posts);
        }
    });
    document.querySelector('#btn-submit-update').disabled = true;
    document.querySelector('#txt-edit-title').disabled = true;
    document.querySelector('#txt-edit-body').disabled = true;
    document.querySelector('#txt-edit-id').value = null;
    document.querySelector('#txt-edit-title').value = null;
    document.querySelector('#txt-edit-body').value = null;
});

const deletePost = (val) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${val}`,{
        method:"DELETE"
    }).then(response => response.json()).then(data => {
        console.log(data);
        let index = posts.findIndex(x => x.id === val);
        if(index > -1) {
            posts.splice(index,1);
        }
        showPosts(posts)
    });
};